patterns = {
    "MONEY": [{
        "TEXT": {
            "IN": [
                "$",
                "€",
                "£",
                "¥",
                "₺"
            ]
        }
    },
        {
            "LIKE_NUM": True
    },
        {
        "POS": "SYM",
        "OP": "?"
    },
        {
        "POS": {"IN": ["NUM", "NOUN"]},
        "IS_ALPHA": True
    }
    ],
    "AMOUNT": [{
        "LIKE_NUM": True
    },
        {
        "POS": {"IN": ["NOUN", "PROPN"]}
    },
        {
        "POS": "SYM",
        "OP": "?"
    },
        {
        "POS": {"IN": ["NOUN", "PROPN"]},
        "OP":"?"
    }],
    "ORG": [{
        "ENT_TYPE": "ORG",
        "POS": {"NOT_IN": ["VERB"]},
        "OP": "+"
    }],
    "ORG-LIKE": [
        {"POS": "PROPN",
         "IS_UPPER": True,
         "IS_ALPHA": True,
         "ENT_TYPE": {"NOT_IN": ["GPE", "PERSON", "ORG"]},
         "OP":"?"
         }
    ],
    "LOCATION": [
        {"ENT_TYPE": "GPE",
         "IS_TITLE": True,
         "OP": "+"}
    ],
    "DATE": [
        {"TEXT": {"REGEX": r"^\d{1,2}[\.\/]\d{1,2}[\.\/](\d{4}|\d{2})$"}}
    ],
    "YEAR": [
        {"TEXT": {"REGEX": r"^(19|20)\d{2}$"}}
    ],
    "PERSON": [
        {"ENT_TYPE": "PERSON",
         "IS_TITLE": True,
         "IS_ALPHA": True,
         "OP": "+"}
    ],
    "CHEMICAL": [
        {"POS": {"NOT_IN": ["PRON", "ADP"]},
         "ENT_TYPE":{"NOT_IN": ["GPE"]},
         "TEXT": {
            "REGEX": r"^(\d{0,2}(He|Li|Be|Ne|Na|Mg|Al|Si|Cl|Ar|Ca|Sc|Ti|Cr|Mn|Fe|Co|Ni|Cu|Zn|Ga|Ge|As|Se|Br|Kr|Rb|Sr|Zr|Nb|Mo|Tc|Ru|Rh|Pd|Ag|Cd|In|Sn|Sb|Te|Xe|Cs|Ba|La|Ce|Pr|Nd|Pm|Sm|Eu|Gd|Tb|Dy|Ho|Er|Tm|Yb|Lu|Hf|Ta|Re|Os|Ir|Pt|Au|Hg|Tl|Pb|Bi|Po|At|Rn|Fr|Ra|Ac|Th|Pa|Np|Pu|Am|Cm|Bk|Cf|Es|Fm|Md|No|Lr|Rf|Db|Sg|Bh|Hs|Mt|Ds|Rg|Cn|Nh|Fl|Mc|Lv|Ts|Og|H|B|C|N|O|F|P|S|K|V|Y|I|U|W)\d{0,2}\.?)+$"}},
    ],
    "CHEMICAL NAME": [
        {"OP": "+",
         "POS": {"IN": ["NOUN"]},
         "TEXT": {
             "REGEX": r"(?i)(hydrogen|helium|lithium|beryllium|boron|carbon|nitrogen|oxygen|fluorine|neon|sodium|magnesium|aluminium|silicon|phosphorus|sulfur|chlorine|argon|potassium|calcium|scandium|titanium|vanadium|chromium|manganese|iron|cobalt|nickel|copper|zinc|gallium|germanium|arsenic|selenium|bromine|krypton|rubidium|strontium|yttrium|zirconium|niobium|molybdenum|technetium|ruthenium|rhodium|palladium|silver|cadmium|indium|tin|antimony|tellurium|iodine|xenon|caesium|barium|lanthanum|cerium|praseodymium|neodymium|promethium|samarium|europium|gadolinium|terbium|dysprosium|holmium|erbium|thulium|ytterbium|lutetium|hafnium|tantalum|tungsten|rhenium|osmium|iridium|platinum|gold|mercury|thallium|lead|bismuth|polonium|astatine|radon|francium|radium|actinium|thorium|protactinium|uranium|neptunium|plutonium|americium|curium|berkelium|californium|einsteinium|fermium|mendelevium|nobelium|lawrencium|rutherfordium|dubnium|seaborgium|bohrium|hassium|meitnerium|darmstadtium|roentgenium|copernicium|nihonium|flerovium|moscovium|livermorium|tennessine|oganesson)"
         }}
    ],
    "PLANT CONSTRUCTION": [
        {"POS": "NOUN", "OP": "+"},
        {"LOWER": {"IN": ["plant", "plants"]}}
    ],
    "PRODUCTION": [
        {"LOWER": "production"},
        {"LOWER": "of"},
        {"POS": {"IN": ["ADJ", "NOUN", "PROPN"]}, "OP":"+"},
    ]
}

patterns_de = {
    "MONEY": [{
        "TEXT": {
            "IN": [
                "$",
                "€",
                "£",
                "¥",
                "₺"
            ]
        }
    },
        {
            "LIKE_NUM": True
    },
        {
        "POS": "SYM",
        "OP": "?"
    },
        {
        "POS": {"IN": ["NUM", "NOUN"]},
        "IS_ALPHA": True
    }
    ],
    "AMOUNT": [{
        "LIKE_NUM": True
    },
        {
        "POS": {"IN": ["NOUN", "PROPN"]}
    },
        {
        "POS": "SYM",
        "OP": "?"
    },
        {
        "POS": {"IN": ["NOUN", "PROPN"]},
        "OP":"?"
    }],
    "ORG": [{
        "ENT_TYPE": "ORG",
        "POS": {"NOT_IN": ["VERB"]},
        "OP": "+"
    }],
    "ORG-LIKE": [
        {"POS": "PROPN",
         "IS_UPPER": True,
         "IS_ALPHA": True,
         "ENT_TYPE": {"NOT_IN": ["GPE", "PERSON", "ORG"]},
         "OP":"?"
         }
    ],
    "LOCATION": [
        {"ENT_TYPE": "GPE",
         "IS_TITLE": True,
         "OP": "+"}
    ],
    "DATE": [
        {"TEXT": {"REGEX": r"^\d{1,2}[\.\/]\d{1,2}[\.\/](\d{4}|\d{2})$"}}
    ],
    "YEAR": [
        {"TEXT": {"REGEX": r"^(19|20)\d{2}$"}}
    ],
    "PERSON": [
        {"ENT_TYPE": "PERSON",
         "IS_TITLE": True,
         "IS_ALPHA": True,
         "OP": "+"}
    ],
    "CHEMICAL": [
        {"POS": {"NOT_IN": ["PRON", "ADP"]},
         "ENT_TYPE":{"NOT_IN": ["GPE"]},
         "TEXT": {
            "REGEX": r"^(\d{0,2}(He|Li|Be|Ne|Na|Mg|Al|Si|Cl|Ar|Ca|Sc|Ti|Cr|Mn|Fe|Co|Ni|Cu|Zn|Ga|Ge|As|Se|Br|Kr|Rb|Sr|Zr|Nb|Mo|Tc|Ru|Rh|Pd|Ag|Cd|In|Sn|Sb|Te|Xe|Cs|Ba|La|Ce|Pr|Nd|Pm|Sm|Eu|Gd|Tb|Dy|Ho|Er|Tm|Yb|Lu|Hf|Ta|Re|Os|Ir|Pt|Au|Hg|Tl|Pb|Bi|Po|At|Rn|Fr|Ra|Ac|Th|Pa|Np|Pu|Am|Cm|Bk|Cf|Es|Fm|Md|No|Lr|Rf|Db|Sg|Bh|Hs|Mt|Ds|Rg|Cn|Nh|Fl|Mc|Lv|Ts|Og|H|B|C|N|O|F|P|S|K|V|Y|I|U|W)\d{0,2}\.?)+$"}},
    ],
    "CHEMICAL NAME": [
        {"OP": "+",
         "POS": {"IN": ["NOUN"]},
         "TEXT": {
             "REGEX": r"(?i)(wasserstof|helium|lithium|beryllium|bor|kohlenstof|stickstof|sauerstof|fluor|neon|natrium|magnesium|aluminium|silicium|phosphor3|schwefe|chlor|argon|kalium|calcium|scandium|titan|vanadium|chrom|mangan|eise|cobalt|nickel|kupfe|zink|gallium|germanium|arsen|selen|brom|krypton|rubidium|strontium|yttrium|zirconium|niob|molybdän|technetium|ruthenium|rhodium|palladium|silbe|cadmium|indium|zin|antimo|tellur|iod|xenon|caesium|barium|lanthan|cer|praseodym|neodym|promethium|samarium|europium|gadolinium|terbium|dysprosium|holmium|erbium|thulium|ytterbium|lutetium|hafnium|tantal|wolfram|rhenium|osmium|iridium|platin|gold|quecksilbe|thallium|ble|bismu|polonium|astat|radon|francium|radium|actinium|thorium|protactinium|uran|neptunium|plutonium|americium|curium|berkelium|californium|einsteinium|fermium|mendelevium|nobelium|lawrencium|rutherfordium|dubnium|seaborgium|bohrium|hassium|meitnerium|darmstadtium|roentgenium|copernicium|nihonium|flerovium|moscovium|livermorium|tenness|oganesson)"
         }}
    ],
    "PLANT CONSTRUCTION": [
        {"LOWER": {"IN": ["bau", "baut"]}},
        {"POS": "NOUN", "OP": "+"},
    ],
    "PRODUCTION": [
        {"LOWER": "herstellung"},
        {"LOWER": "von", "OP": "?"},
        {"POS": {"IN": ["ADJ", "NOUN", "PROPN"]}, "OP":"+"},
    ]
}