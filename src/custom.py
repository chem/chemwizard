#!env/bin/python
# -*- encoding:utf-8 -*-

import sys

from atlasdriver import Driver
from helper import Helper


class Custom:
    """The Custom class provides several automation methods for collecting the article content and analyzing the article content.

    Attributes:
        db (Driver): The Driver instance.
        helper (Helper): The Helper instance.
    """

    def __init__(self):
        """Initialize a Custom instance.
        """
        self.db = Driver()
        self.helper = Helper()

    def crawl(self, url):
        """This method automates the process of extracting the content, matching entities and inserting the created entities into the database.

        Args:
            url (str): The URL of the article.
        """
        domain = self.helper.domain_name(url)
        print("Extracting content...")
        content = self.helper.universal_extractor(url)

        print("Getting matches...")
        matches = self.helper.smart_linker(content)

        print("Creating entities...")
        article, entities = self.helper.create_entities(domain, url, matches)

        print("Inserting entities...")
        article_id = self.db.insert_entity(article)
        for entity in entities:
            entity_id = self.db.insert_entity(entity)
            self.db.insert_link("contains", article_id, entity_id)
        print("done.")


if __name__ == "__main__":
    arg = sys.argv[1:]
    if not arg:
        exit()
    url = arg[0]
    c = Custom()
    c.crawl(url)
