#!env/bin/python
# -*- encoding:utf-8 -*-

import json

from atlasdriver import Driver
from helper import Helper


class Crawler:
    """The Crawler class provides several automation methods for collecting article links and analyzing the article contents.

    Attributes:

        counter (int): A simple counter.
        thread_count (int): For counting threads.
        base_url (str): The homepage URL of the website.
        loaded_urls (list): A list containing the loaded urls.
        article_urls (list): A list containing article urls.
        article_contents (dict): A dict containing article urls as keys an contents as values.
        chemicals (dict): A dict containing some chemical names as keys and cids as values.
        label_filter (list): A list containing some spaCy entity types for filtering out.
        config (dict): A dictionary containing the config of this website from the ``config.json``.
        topic (str): The topic selection of the news section.
        search_url (str): The URL used to view articles using paging.
        db (Driver): The Driver instance.
        helper (Helper): The Helper instance.
    """

    def __init__(self):
        """Initialize a Crawler instance.
        """
        # Initial variables
        self.counter = 0
        self.thread_count = 0
        self.base_url = "https://www.chemietechnik.de"

        self.loaded_urls = []
        self.article_urls = []
        self.article_contents = {}
        self.chemicals = {}

        self.label_filter = ['ORG', 'GPE', 'DATE', 'TIME', 'QUANTITY',
                             'MONEY', 'FACILITY', 'PERCENT', 'LOC', 'NORP', 'PRODUCT']

        # Load Config
        self.config = {}
        self.load_config()

        # Select topic
        self.select_topic("markt")

        # Database Connection
        self.db = Driver()
        self.helper = Helper()

        # Calculate the highest page if necessary
        if self.config['chemietechnik']['highest_page'] == 0:
            print("Calculating highest page...")
            self.config['chemietechnik']['highest_page'] = self.helper.bin_search_chemietechnik(
                self.search_url)
            self.save_config()

        # START PROCESSES
        # ===============

        # 1. Collect article urls
        if len(self.loaded_urls) == 0:
            print("Collecting total number of %s pages..." %
                  (self.config['chemietechnik']['page_limit']))
            self.get_articles()

        # 2. Extract information from articles
        print("Parsing total number of %s articles..." %
              (self.config['chemietechnik']['article_limit']))
        self.parse_articles()

        # 3. Analyze information from articles
        print("Analyzing total number of %s articles..." %
              (self.config['chemietechnik']['article_limit']))
        print("This may take a while...")
        self.analyze_contents()

        # 4. Save last run
        self.db.update_last_run("chemietechnik")

    # Load the config file
    def load_config(self):
        """Load the config for this website from ``config.json``
        """
        with open("config.json", ) as f:
            self.config = json.load(f)
        print("Using the following config:")
        print("===========================")
        print("Page Limit:", self.config['chemietechnik']['page_limit'])
        print("Article Limit:", self.config['chemietechnik']['article_limit'])
        print("===========================")

    # Save the changes to the config file
    def save_config(self):
        """Save the changes to the config for this website to ``config.json``
        """
        with open("config.json", "w") as f:
            json.dump(self.config, f, indent=4)

    # Specify search url via the selected topic
    def select_topic(self, topic):
        """Select the topic for this website, which also sets the ``topic`` and the ``search_url`` variables according to the selection.

        Args:
            topic (str): A topic identifier visible in the website's news section.
        """
        self.topic = topic
        self.search_url = self.config['chemietechnik']['search_topics'][topic]

    # Iterate through paging and get collect article urls
    def get_articles(self):
        """Iterate through paging and collect article urls using the ``fetch_article_chemietechnik`` method from ``Helper`` class.

        Once the helper function returns URLs, this method filters out repeating URLs and stores them in loaded_urls.
        """
        limit = self.config['chemietechnik']['page_limit']
        for i in range(limit):
            print(f"Running on page {i}")
            search = f"{self.search_url}{i}"
            urls = self.helper.fetch_article_chemietechnik(search)
            self.article_urls += urls

        self.loaded_urls = list(set(self.article_urls))
        print("Collected %s articles." % (len(self.loaded_urls)))

    # Iterate through collected article urls and save the extracted content
    def parse_articles(self):
        """Iterate through loaded article urls and extract content using the ``universal_extractor`` method from ``Helper`` class.

        Once the helper function returns the content, this method stores contents in ``article_contents`` with the URL of the article being the key.
        """
        limit = self.config['chemietechnik']['article_limit']
        for i in range(limit):
            print(f"Running on article {i}")
            url = self.loaded_urls[i]
            content = self.helper.universal_extractor(url)

            if content:
                self.article_contents[url] = {
                    "content": content}

        print("done.")

    # Iterate through parsed articles and analyze chemicals and entities
    def analyze_contents(self):
        """Iterate through parsed articles and analyze the content and create entities using the ``smart_linker`` and the ``create_entities`` methods from the ``Helper`` class.

        Note:
            Only distinct entities are inserted into the database. If there is already an entity with the same values in the database, then the entity will not be inserted twice.

        After calling this function all entities found in the content will be added to the database.
        """
        keys = list(self.article_contents.keys())
        print(f"Total articles to be analyzed: {len(keys)}")
        for key in keys:
            # Check if article was already analyzed before
            if self.db.is_article_analyzed("chemietechnik", key):
                print("Article already analyzed, passing...")
                continue

            print(f"Analyzing [{key}]")
            matches = self.helper.smart_linker_de(
                self.article_contents[key]['content'])
            article, entities = self.helper.create_entities(
                "chemietechnik", key, matches)

            article_id = self.db.insert_entity(article)
            for entity in entities:
                entity_id = self.db.insert_entity(entity)
                self.db.insert_link("contains", article_id, entity_id)


if __name__ == "__main__":
    Crawler()
