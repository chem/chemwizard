import json
from datetime import datetime
from urllib.parse import urlparse

from dateutil import parser
import requests
import spacy
from bs4 import BeautifulSoup
from lxml import html
from spacy.matcher import Matcher

from patterns import patterns, patterns_de

spamFilter = [
    "Copyright",
    "IHS Markit (NYSE:INFO)",
    "IHS Markit (NYSE: INFO)"
]

organization_instances = ["Q4830453", "Q891723", "Q6881511", "Q783794"]
person_instances = ["Q5"]


class Helper:
    """The Helper class provides several methods for crawling, text extraction and entity recognition.

    Attributes:
        nlp (Language): A Language object from spacy with the loaded pipeline.  
        matcher (Matcher): A Matcher object from spacy, which matches sequences of tokens, based on pattern rules
    """

    def __init__(self):
        """Initialize a Helper instance.
        """
        self.nlp = spacy.load("en_core_web_trf")
        self.matcher = Matcher(self.nlp.vocab)

        self.nlp_de = spacy.load("de_dep_news_trf")
        self.matcher_de = Matcher(self.nlp_de.vocab)

        # Load patterns
        for pattern in patterns.keys():
            self.matcher.add(
                pattern, [patterns[pattern]], greedy="LONGEST")

        # Load patterns
        for pattern in patterns_de.keys():
            self.matcher_de.add(
                pattern, [patterns_de[pattern]], greedy="LONGEST")

    def fetch_article_chemanager(self, url):
        """Collect article urls from paging view for chemanager.

        Note:
            Not all urls may start with http/https and the domain name.

        Args:
            url (str): The URL of the page that contains articles.

        Returns:
            urls (list): A list containing article urls.
        """
        page = requests.get(url).content
        tree = html.fromstring(page)
        urls = tree.xpath(
            '//div[@class="views-row"]//div[contains(@class, "teaser-info")]/h2/a/@href')
        return urls

    def fetch_article_ihsmarkit(self, url):
        """Collect article urls from paging view for ihsmarkit.

        Note:
            All urls start with http/https and the domain name.

        Args:
            url (str): The URL of the page that contains articles.

        Returns:
            urls (list): A list containing article urls.
        """
        page = requests.get(url).content
        tree = html.fromstring(page)
        urls = tree.xpath(
            '//div[@class="view-content"]//a/@href')
        fixed = []
        for url in urls:
            if not url.startswith("http"):
                fixed.append(f"https://news.ihsmarkit.com{url}")
            else:
                fixed.append(url)
        return fixed

    def fetch_article_chemietechnik(self, url):
        """Collect article urls from paging view for chemietechnik.

        Note:
            All urls start with http/https and the domain name.

        Args:
            url (str): The URL of the page that contains articles.

        Returns:
            urls (list): A list containing article urls.
        """
        page = requests.get(url).content
        tree = html.fromstring(page)
        urls = tree.xpath(
            '//div[@class="post-list topicbox"]/a[@class="postlist-item media d-flex"]/@href')
        return urls

    def bin_search_chemanager(self, search_url):
        """Find the total number of webpages using binary search for chemanager.

        Args:
            search_url (str): The URL of the page to run the search on, which usually contains a paging view.

        Returns:
            int: The paging number of the highest page.
        """
        low = 0
        high = 0
        marker = 0

        while True:
            page = requests.get(f"{search_url}{high}").content
            tree = html.fromstring(page)

            if len(tree.find_class("views-infinite-scroll-content-wrapper")) == 1:
                high += 500
            else:
                break

        marker = int((high-low)/2)

        while True:
            if low == marker or abs(low-marker) in range(5):
                break

            page = requests.get(f"{search_url}{marker}").content
            tree = html.fromstring(page)

            if len(tree.find_class("views-infinite-scroll-content-wrapper")) == 1:
                low = marker
                marker += int((high-low)/2)
            else:
                high = marker
                marker -= int((high-low)/2)
        return marker

    def bin_search_chemietechnik(self, search_url):
        """Find the total number of webpages using binary search for chemietechnik.

        Args:
            search_url (str): The URL of the page to run the search on, which usually contains a paging view.

        Returns:
            int: The paging number of the highest page.
        """
        low = 0
        high = 0
        marker = 0

        while True:
            page = requests.get(f"{search_url}{high}").content
            tree = html.fromstring(page)

            if len(tree.find_class("postlist-meta")):
                high += 500
            else:
                break

        marker = int((high-low)/2)

        while True:
            if low >= marker:
                break

            page = requests.get(f"{search_url}{marker}").content
            tree = html.fromstring(page)

            if len(tree.find_class("postlist-meta")):
                low = marker
                marker += int((high-low)/2)+1
            else:
                high = marker
                marker -= int((high-low)/2)+1
        return marker

    def has_ihs_page(self, tag):
        """A method to be used with BeautifulSoup to filter certain tags for ihsmarkit.

        Args:
            tag (Tag): A Tag object corresponds to an XML or HTML tag in the original document as defined in BeautifulSoup.

        Returns:
            True if the tag matches the rules, False otherwise.
        """
        if tag.name == 'a' and tag.get('href').startswith('https://news.ihsmarkit.com/INFO/press_releases_iframe/view/0/time/DESC/'):
            return True
        return False

    def has_icis_page(self, tag):
        """A method to be used with BeautifulSoup to filter certain tags for icis.

        Args:
            tag (Tag): A Tag object corresponds to an XML or HTML tag in the original document as defined in BeautifulSoup.

        Returns:
            True if the tag matches the rules, False otherwise.
        """
        if tag.name == 'a' and tag.get('href').startswith('https://www.icis.com/explore/resources/news-library?page_number='):
            return True
        return False

    def bin_search_ihsmarkit(self, search_url):
        """Find the total number of webpages for ihsmarkit.

        Note:
            In comparison to the ``bin_search_chemanager`` method, this one does not use binary search and the keyword **bin** can the name of this function can be considered as **Searching the recycling bin** instead of **Binary search**.

        Args:
            search_url (str): The URL of the page to run the search on, which usually contains a paging view.

        Returns:
            int: The paging number of the highest page.
        """
        r = requests.get(search_url)
        page = r.content
        soup = BeautifulSoup(page, 'html.parser')
        links = soup.find_all(self.has_ihs_page, recursive=True)
        last = links[-1].get('href').split(
            "https://news.ihsmarkit.com/INFO/press_releases_iframe/view/0/time/DESC/")[-1].split("?")[0]
        return int(last)/10

    def bin_search_icis(self, search_url):
        """Find the total number of webpages for icis.

        Note:
            In comparison to the ``bin_search_chemanager`` method, this one does not use binary search and the keyword **bin** can the name of this function can be considered as **Searching the recycling bin** instead of **Binary search**.

        Args:
            search_url (str): The URL of the page to run the search on, which usually contains a paging view.

        Returns:
            int: The paging number of the highest page.
        """
        r = requests.get(search_url)
        page = r.content
        soup = BeautifulSoup(page, 'html.parser')
        links = [link.get('href') for link in soup.find_all(
            self.has_icis_page, recursive=True)]
        links.sort()
        return int(links[-1].rsplit("=")[-1])

    def universal_extractor(self, url):
        """Extract content universally from any article.

        Args:
            url (str): The URL of the article.

        Returns:
            str: The content of the article.
            None: If the URL is not accessible.
        """
        r = requests.get(url)
        if not r.status_code == 200:
            return None

        page = r.content
        soup = BeautifulSoup(page, 'html.parser')
        elems = soup.body.find_all(self.isArticleText, recursive=True)

        contents = [self.extract(elem) for elem in elems]
        elems = list(filter(self.filterNonsense, contents))

        return " ".join(elems)

    def isArticleText(self, tag):
        """Filter for excluding elements that can not be identified as an article text.

        Args:
            tag (Tag): A Tag object corresponds to an XML or HTML tag in the original document as defined in BeautifulSoup.

        Returns:
            True if the tag matches the rules, False otherwise.
        """
        if tag.name in ["p", "h1"] and tag.text:
            return True
        return False

    def extract(self, tag):
        """Extract text content from tag.

        Args:
            tag (Tag): A Tag object corresponds to an XML or HTML tag in the original document as defined in BeautifulSoup.

        Returns:
            str: Text content of the tag.
        """
        text = ""
        matches = tag.find_all(text=True, recursive=False)
        for match in matches:
            for line in match.splitlines():
                strippedLine = line.strip()
                if strippedLine:
                    text += f"{strippedLine} "
        return text

    def filterNonsense(self, obj):
        """Filter for excluding None objects and unimportant junk text.

        Args:
            obj (None or str): Object to test out.

        Returns:
            True if the object matches the rules, False otherwise.
        """
        if not obj or len(obj) < 3 or len(obj.split()) <= 2:
            return False
        if any([obj.startswith(exc) for exc in spamFilter]):
            return False
        return True

    def smart_linker(self, content):
        """Smart Entity Linker using Rule-Based Matching. This method tokenizes the text and identifies tokens that can be categorized into entities. The resulting dictionary consists of 12 distinct entity types, which are lists containing the matched tokens.

        Args:
            content (str): Content to be analyzed.

        Returns:
            dict: A dictionary containing 12 keys with each key holding a list of tokens.
        """
        entities = {
            "MONEY": [],
            "ORG": [],
            "ORG-LIKE": [],
            "LOCATION": [],
            "DATE": [],
            "YEAR": [],
            "PERSON": [],
            "CHEMICAL": [],
            "CHEMICAL NAME": [],
            "PLANT CONSTRUCTION": [],
            "PRODUCTION": [],
            "AMOUNT": []
        }
        doc = self.nlp(content)
        matches = self.matcher(doc)
        for match_id, start, end in matches:
            # Get string representation
            string_id = self.nlp.vocab.strings[match_id]
            span = doc[start:end]  # The matched span
            if span.text not in entities[string_id]:
                entities[string_id].append(span.text)
        return entities

    def smart_linker_de(self, content):
        """Smart Entity Linker for the German language using Rule-Based Matching. This method tokenizes the text and identifies tokens that can be categorized into entities. The resulting dictionary consists of 12 distinct entity types, which are lists containing the matched tokens.

        Args:
            content (str): Content to be analyzed.

        Returns:
            dict: A dictionary containing 12 keys with each key holding a list of tokens.
        """
        entities = {
            "MONEY": [],
            "ORG": [],
            "ORG-LIKE": [],
            "LOCATION": [],
            "DATE": [],
            "YEAR": [],
            "PERSON": [],
            "CHEMICAL": [],
            "CHEMICAL NAME": [],
            "PLANT CONSTRUCTION": [],
            "PRODUCTION": [],
            "AMOUNT": []
        }
        doc = self.nlp_de(content)
        matches = self.matcher_de(doc)
        for match_id, start, end in matches:
            # Get string representation
            string_id = self.nlp_de.vocab.strings[match_id]
            span = doc[start:end]  # The matched span
            if span.text not in entities[string_id]:
                entities[string_id].append(span.text)
        return entities

    # Get domain name from url
    def domain_name(self, url):
        """Get domain name from an URL.

        Args:
            url (str): A URL of a website.

        Returns:
            str: Domain name of a URL without the ``www`` subdomain.
        """
        domain = urlparse(url).netloc
        return domain.split('www.')[-1]

    # Entity Information Gathering Methods #
    #======================================#

    def get_label_of_entry(self, id):
        """Get the english label of a Wikidata entry.

        Args:
            id (str): A Wikidata entity ID, also known as Q-item.

        Returns:
            str: The english label of the entity.
            None: If the page is not accessible.
        """
        # Load wikidata entity page
        r = requests.get(
            f"https://www.wikidata.org/wiki/Special:EntityData/{id}.json")
        if not r.status_code == 200:
            return None

        result = json.loads(r.content)
        labels = result['entities'][id]['labels']

        if 'en' in labels.keys():
            return labels['en']['value']

    def wiki_search(self, text):
        """Search wikidata for an entry and return some information.

        Args:
            text (str): The search query, usually just a name of an entry.

        Returns:
            tuple: A tuple containing Wikidata ID, description, labels and claims in order.
            None: If the page is not accessible or no results are found.
        """
        endpoint = "https://wikidata.org/w/api.php"
        params = {
            "action": "query",
            "list": "search",
            "format": "json",
            "srsearch": text
        }
        # Make Wikidata Search
        r = requests.get(endpoint, params)
        if not r.status_code == 200:
            return None

        results = json.loads(r.content)["query"]["search"]
        if not len(results):
            return None

        # Get Wikidata ID
        res = results[0]
        if not 'title' in res.keys():
            return None
        _id = res['title']

        # Get Description
        if 'snippet' in res.keys():
            _desc = res['snippet']

        # Load wikidata entity page
        r = requests.get(
            f"https://www.wikidata.org/wiki/Special:EntityData/{_id}.json")
        if not r.status_code == 200:
            return None

        # Get page information
        result = json.loads(r.content)
        labels = result['entities'][_id]['labels']
        claims = result['entities'][_id]['claims']

        return (_id, _desc, labels, claims)

    def get_chemical(self, text):
        """Get information about a chemical using the PubChem PUG REST.

        Args:
            text (str): A string containing a formula or the name of chemical compund.

        Returns:
            tuple: A tuple containing the cid, the title and the description of the chemical in order.
            None: If no chemicals are found.
        """

        r = requests.get(
            f"https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/{text}/description/json", timeout=3)
        if not r.status_code == 200:
            return None

        chem = json.loads(r.content)
        cid, title = chem['InformationList']['Information'][0].values()
        if not (len(chem['InformationList']['Information']) >= 2 and 'Description' in chem['InformationList']['Information'][1].keys()):
            return None

        desc = chem['InformationList']['Information'][1]['Description']

        return (cid, title, desc)

    def get_unixtime(self, datetext):
        """Date to Unixtime Conversion.

        Takes a year or date in format dd./mm./yy OR dd./mm./yyyy and returns UTC Unixtime in miliseconds.

        Args:
            datetext (str): A string containing a date in human readable form.

        Returns:
            float: Unixtime in miliseconds.
        """
        if len(datetext) == 4:
            dt = datetime(int(datetext), 1, 1)
            return dt.timestamp()*1000

        nums = datetext.replace('/', '.').split('.')
        dt = datetime(int(nums[2]), int(nums[1]), int(nums[0]))
        return dt.timestamp()*1000

    def get_iso_unixtime(self, datetext):
        """Convert ISO 8601 datetime format into unixtime.

        Args:
            datatext (str): A string containing ISO 8601 datetime.

        Returns:
            float: Unixtime in miliseconds.
            0: If the input raises an error.
        """
        try:
            dt = parser.isoparse(datetext[1:])
            return dt.timestamp()*1000
        except:
            return 0

    def get_org(self, text):
        """Get information about an organisation using the Wikidata.

        Args:
            text (str): The name of an organisation.

        Returns:
            tuple: A tuple containing Wikidata ID, name, description, inception, founder, country and industry in order.
            None: If the given keywords do not represent a company or raises an error.
        """
        _id = ""
        _name = ""
        _desc = ""
        _inception = ""
        _founders = ""
        _country = ""
        _industries = ""

        ##
        response = self.wiki_search(text)
        if not response:
            return None
        _id, _desc, labels, claims = response
        ##

        if "P31" not in claims.keys():
            return None

        # Check instance_of
        org = False
        for claim in claims['P31']:
            if claim['mainsnak']['datavalue']['value']['id'] in organization_instances:
                org = True
                break

        if not org:
            return None

        # Get Name
        if 'en' in labels.keys():
            _name = labels['en']['value']

        # Get Inception
        if 'P571' in claims.keys():
            _inception = self.get_iso_unixtime(
                claims['P571'][0]['mainsnak']['datavalue']['value']['time'])

        # Get Founders
        if 'P112' in claims.keys():
            _founders = self.get_label_of_entry(
                claims['P112'][0]['mainsnak']['datavalue']['value']['id'])

        # Get Country
        if 'P17' in claims.keys():
            _country = self.get_label_of_entry(
                claims['P17'][0]['mainsnak']['datavalue']['value']['id'])

        # Get Industry
        if 'P452' in claims.keys():
            _industries = self.get_label_of_entry(
                claims['P452'][0]['mainsnak']['datavalue']['value']['id'])

        return (_id, _name, _desc, _inception, _founders, _country, _industries)

    def get_person(self, text):
        """Get information about a person using the Wikidata.

        Args:
            text (str): The name of a person.

        Returns:
            tuple: A tuple containing Wikidata ID, name, description, gender, dateofbirth and citizenship in order.
            None: If the given keywords do not represent a person or raises an error.
        """
        _id = ""
        _name = ""
        _gender = ""
        _citizenship = ""
        _dateofbirth = ""
        _desc = ""

        ##
        response = self.wiki_search(text)
        if not response:
            return None
        _id, _desc, labels, claims = response
        ##

        if "P31" not in claims.keys():
            return None

        # Check instance_of
        human = False
        for claim in claims['P31']:
            if claim['mainsnak']['datavalue']['value']['id'] in person_instances:
                human = True
                break

        if not human:
            return None

        # Get Name
        if 'en' in labels.keys():
            _name = labels['en']['value']

        # Get Gender
        if 'P21' in claims.keys():
            claim = claims['P21'][0]['mainsnak']['datavalue']['value']['id']
            if claim == "Q6581097":
                _gender = "male"
            elif claim == "Q6581072":
                _gender = "female"

        # Get Citizenship
        if 'P27' in claims.keys():
            _citizenship = self.get_label_of_entry(
                claims['P27'][0]['mainsnak']['datavalue']['value']['id'])

        # Get Date of Birth
        if 'P569' in claims.keys():
            _dateofbirth = claims['P569'][0]['mainsnak']['datavalue']['value']['time']

        return (_id, _name, _desc, _gender, _dateofbirth, _citizenship)

    def get_location(self, text):
        """Get information about a location using the Wikidata.

        Args:
            text (str): The name of a location.

        Returns:
            tuple: A tuple containing Wikidata ID, name, description, country and coordinates in order.
            None: If the given keywords do not return a result from the search.
        """
        _id = ""
        _name = ""
        _desc = ""
        _country = ""
        _coordinates = ""

        ##
        response = self.wiki_search(text)
        if not response:
            return None
        _id, _desc, labels, claims = response
        ##

        # Get Name
        if 'en' in labels.keys():
            _name = labels['en']['value']

        # Get Country
        if 'P17' in claims.keys():
            _country = self.get_label_of_entry(
                claims['P17'][0]["mainsnak"]["datavalue"]["value"]["id"])

        # Get Coordinates
        if 'P625' in claims.keys():
            latitude = claims['P625'][0]["mainsnak"]["datavalue"]["value"]["latitude"]
            longitude = claims['P625'][0]["mainsnak"]["datavalue"]["value"]["longitude"]
            _coordinates = f"{latitude}+{longitude}"

        return (_id, _name, _desc, _country, _coordinates)

    # Create Entities for Matches

    def create_entities(self, website, url, matches):
        """Create entities from matches.

        This method creates a dictionary for each found entity in the matches. Each entity dictionary has at least a type and some additional attributes. These articles are later inserted into a list and returned.

        Args:
            website (str): The website identifier of the entities.
            url (str): The URL of the article that yields these entities.
            matches (dict): A dictionary containing 12 distinct entity keys, generated from ``smart_linker`` method.

        Returns:
            dict, list : A dictionary containin the article entity and a list containing other entity dictionaries.
        """

        entities = []
        article = {
            "type": "Article",
            "website": website,
            "url": url,
            "datetime": [],
            "money": matches['MONEY'],
            "construction": matches['PLANT CONSTRUCTION'],
            "production": matches['PRODUCTION'],
            "amount": matches['AMOUNT']
        }

        # Convert and add dates to article entity
        for dt in matches['DATE'] + matches['YEAR']:
            unixtime = self.get_unixtime(dt)
            if unixtime not in article['datetime']:
                article['datetime'].append(self.get_unixtime(dt))

        # Create organization entities
        for org in list(set(matches['ORG']).union(matches['ORG-LIKE'])):
            info = self.get_org(org)
            if info:
                entities.append(
                    {
                        "type": "Organization",
                        "wikidata_id": info[0],
                        "name": info[1],
                        "desc": info[2],
                        "inception": info[3],
                        "founder": info[4],
                        "country": info[5],
                        "industry": info[6]
                    }
                )

        # Create person entities
        for person in matches['PERSON']:
            info = self.get_person(person)
            if info:
                entities.append(
                    {
                        "type": "Person",
                        "wikidata_id": info[0],
                        "name": info[1],
                        "desc": info[2],
                        "gender": info[3],
                        "dateofbirth": info[4],
                        "citizenship": info[5]
                    }
                )

        # Create chemical entities
        for chemical in list(set(matches['CHEMICAL']).union(matches['CHEMICAL NAME'])):
            info = self.get_chemical(chemical)
            if info:
                entities.append(
                    {
                        "type": "Chemical",
                        "cid": info[0],
                        "name": info[1],
                        "desc": info[2]
                    }
                )

        # Create location entities
        for location in matches['LOCATION']:
            info = self.get_location(location)
            if info:
                entities.append(
                    {
                        "type": "Location",
                        "wikidata_id": info[0],
                        "name": info[1],
                        "desc": info[2],
                        "country": info[3],
                        "coordinates": info[4]
                    }
                )

        return article, entities