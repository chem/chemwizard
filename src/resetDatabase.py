#!env/bin/python
# -*- encoding:utf-8 -*-

import pymongo
import os

collections = ["entities", "stats", "links"]


class ConnectionError(Exception):
    """Raised when the connection to the database can't be established.
    """
    pass


class Driver:
    def __init__(self):
        self.db = self.connect()
        if (self.db == None):
            raise ConnectionError(
                "Can't connect to the database. Check your connection string.")

    def connect(self):
        connection_string = os.environ['MONGODB']
        client = pymongo.MongoClient(connection_string)
        db = client.data
        return db

    def reset(self):
        cols = self.db.list_collection_names()
        for col in cols:
            print(f"[Reset] Dropping collection {col}...")
            self.db.drop_collection(col)
        self.setup()

    def setup(self):
        if (self.db.list_collection_names() == collections):
            return

        for col in collections:
            print(f"[Setup] Creating collection {col}...")
            self.db.create_collection(col)

        self.db.stats.insert_one({
            "id": 0,
            "last_updated": {
                "chemanager": 0,
                "chemietechnik": 0,
                "icis": 0,
                "ihsmarkit": 0
            }
        })


if __name__ == "__main__":
    dr = Driver()
    dr.reset()
