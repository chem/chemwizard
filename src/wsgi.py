#!env/bin/python
# -*- encoding:utf-8 -*-

import os
import logging
from subprocess import Popen, PIPE
from flask_cors import CORS
from flask import Flask
from flask_socketio import SocketIO, emit
from atlasdriver import Driver
import webbrowser

os.environ["PYTHONUNBUFFERED"] = "1"

log = logging.getLogger('werkzeug')
log.disabled = True

app = Flask(__name__, template_folder='static')
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 1
app.socketio = SocketIO(app, cors_allowed_origins='*')
app.db = Driver()
CORS(app)

webbrowser.open("../web/index.html", new=2)

# SocketIO


@app.socketio.on('connect')
def connect(data=None):
    """Called after getting a **connect** signal.

    This function emits a **connected** signal once the client establishes a successful connection with the server.

    Emits:
        - **connected**
    """
    emit('connected')


@app.socketio.on('crawlArticle')
def crawArticle(data):
    """Called after getting a **crawlArticle** signal.

    This function runs the custom crawler with the given URL and sends the updates back to the client with the signal **updateCustomLog**. On completion, the server notifies the client with the **finishCustomLog** signal.

    Args:
        data: A JSON data with the key ``url`` *(str)*

    Emits:
        - **updateCustomLog** - A JSON data with keys ``message`` *(str)*
        - **finishCustomLog**
    """
    if 'url' not in data.keys():
        return

    emit("updateCustomLog", {"message": f"> Starting crawler 'custom'..."})
    with Popen([f"./custom.py", data["url"]], stdout=PIPE) as p:
        for line in p.stdout:
            emit('updateCustomLog', {"message": f"..{line.decode().strip()}"})
    emit("updateCustomLog", {"message": f"> Crawler 'custom' finished."})
    emit("finishCustomLog")


@app.socketio.on('getStartGraph')
def get_start_graph():
    """Called after getting a **getStartGraph** signal.

    This function connects to the database and returns the necessary nodes and edges for creating the graph by emitting the data with the signal **plotStartGraph**.

    Emits:
        - **plotStartGraph** - A JSON data with keys ``nodes`` *(list)*, ``edges`` *(list)*
    """
    nodes, edges = app.db.get_start_graph()
    emit("plotStartGraph", {"nodes": nodes, "edges": edges})


@app.socketio.on('getLastUpdates')
def lastUpdates():
    """Called after getting a **getLastUpdates** signal. 

    This function connects to the database and returns the dates of the crawlers' last runs by emitting the data with the signal **lastUpdates**.

    Emits:
        - **lastUpdates** - A JSON data with keys ``status`` *(str)*, ``data`` *(list)*
    """
    rows = app.db.get_last_updates()
    if rows:
        emit("lastUpdates", {"status": "success", "data": rows})


@app.socketio.on('resetDatabase')
def resetDatabase():
    """Called after getting a **resetDatabase** signal.

    This function runs the ``resetDatabase.py`` script to reset the database and clear all documents inside collections. During the run, it returns the updates of the script in real time by emitting the **updateResetLog** signal and on completion it emits the signal **finishResetLog**.

    Emits:
        - **updateResetLog** - A JSON data with keys ``message`` *(str)*
        - **finishResetLog**
    """
    emit("updateResetLog", {"message": f"> Starting reset process..."})
    with Popen([f"./resetDatabase.py"], stdout=PIPE) as p:
        for line in p.stdout:
            emit('updateResetLog', {"message": f"..{line.decode().strip()}"})
    emit("updateResetLog", {"message": f"> Reset process finished."})
    emit("finishResetLog")
    lastUpdates()


@app.socketio.on('runCrawler')
def runCrawler(data):
    """Called after getting a **runCrawler** signal.

    This function runs the crawler specified in the given parameter with the ``crawler`` key. During the run, it returns the updates of the script in real time by emitting the **updateLog** signal and on completion it emits the signal **finishLog**.

    Args:
        data: A JSON data with the key ``crawler`` *(str)*

    Emits:
        - **updateLog** - A JSON data with keys ``message`` *(str)*
        - **finishLog**
    """
    crawler = data['crawler']
    if crawler not in ['chemanager', 'chemietechnik', 'ihsmarkit', 'icis']:
        return

    emit("updateLog", {"message": f"> Starting crawler '{crawler}'..."})
    with Popen([f"./{crawler}.py"], stdout=PIPE) as p:
        for line in p.stdout:
            emit('updateLog', {"message": f"..{line.decode().strip()}"})
    emit("updateLog", {"message": f"> Crawler '{crawler}' finished."})
    emit("finishLog")
    lastUpdates()


@app.socketio.on('getStats')
def stats():
    """Called after getting a **getStats** signal.

    This function connects to the database and returns the statistics of the database by emitting the data with the signal **stats**.

    Emits:
        A JSON data with keys ``status`` *(str)*, ``data`` *(list)*
    """
    rows = app.db.get_stats()
    if rows:
        emit("stats", {"status": "success", "data": rows})
