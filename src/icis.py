#!env/bin/python
# -*- encoding:utf-8 -*-

if __name__ == "__main__":
    print("This crawler is not available, because it is not possible to crawl the ICIS website, for they load the articles dynamically using Javascript. This means that you cannot use this crawler to collect article URLs, however it is still possible to analyze single ICIS articles using the 'Add Article' section, which uses our custom crawler.")