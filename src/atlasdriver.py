import time
import os
import pymongo

collections = ["entities", "stats", "links"]


class ConnectionError(Exception):
    """Raised when the connection to the database can't be established.
    """
    pass


class Driver:
    """The Driver class provides access to MongoDB Atlas.

    Attributes:
        db (Database): A physical container for collections. Each database gets its own set of files on the file system. A single MongoDB server typically has multiple databases.
    """

    def __init__(self):
        """Initialize a Driver instance.
        """
        self.db = self.connect()
        if (self.db == None):
            raise ConnectionError(
                "Can't connect to the database. Check your connection string.")

    def connect(self):
        """Connect to the database and return the database object.

        Returns:
            Database: The database object of the database data, obtained with MongoClient.
        """
        connection_string = os.environ['MONGODB']
        client = pymongo.MongoClient(connection_string)
        db = client.data
        return db

    def get_last_updates(self):
        """Return the dates of the crawlers' last runs.

        Returns:
            dict: A dictionary containing each website as a key.
        """
        doc = self.db.stats.find_one({"id": 0})
        if doc:
            return doc["last_updated"]
        return None

    def get_stats(self):
        """Return some database statistics such as entity count.

        Returns:
            dict: A dictionary with the key ``articleCount``
        """
        stats = {}
        stats["articleCount"] = self.db.entities.estimated_document_count()
        return stats

    def update_last_run(self, website):
        """Update the last updated date of a crawler to the current datetime.

        Note:
            The assigned value to the website is unixtime in miliseconds.

        Args:
            website (str): Identifier of the website.
        """
        dt = int(time.time()*1000)
        self.db.stats.update_one(
            {"id": 0}, {"$set": {f"last_updated.{website}": dt}})

    def insert_entity(self, entity):
        """Check for duplicates and insert the entity if no duplicates are found.

        Args:
            entity (dict): A dictionary containing the entity type and additional attributes as keys.

        Returns:
            ObjectId: The inserted document's _id.
        """
        res = self.db.entities.find_one(entity)
        if res:
            return res['_id']
        res = self.db.entities.insert_one(entity)
        return res.inserted_id

    def insert_link(self, _type, _from, _to):
        """Create a link between two entities.

        Args:
            _type (str): Type of the link.
            _from (ObjectId): _id of the entity that the edge connects from.
            _to (ObjectId): _id of the entity that the edge connects to.
        """
        self.db.links.insert_one(
            {
                "type": _type,
                "from": _from,
                "to": _to
            }
        )

    def is_article_analyzed(self, website, url):
        """Check if the article is already analyzed and inserted into the database.

        Args:
            website (str): The identifier of the website.
            url (str): The URL of the article.

        Returns:
            True if the article is already analyzed, False otherwise.
        """
        res = self.db.entities.find_one({"website": website, "url": url})
        if res:
            return True
        return False

    def get_start_graph(self):
        """Return all article entities as nodes and links as edges.

        Returns:
            list, list: The first list contains the nodes and the second one contains the edges.
        """
        # Find all entitites
        nodes = self.db.entities.aggregate([
            {
                "$addFields": {
                    "id": {"$toString": "$_id"},
                }
            },
            {
                "$project": {
                    "_id": 0
                }
            }
        ])

        # Find all links
        edges = self.db.links.aggregate([
            {
                "$project": {
                    "from": {"$toString": "$from"},
                    "to": {"$toString": "$to"},
                    "_id": 0
                }
            }
        ])
        return list(nodes), list(edges)


if __name__ == "__main__":
    dr = Driver()
