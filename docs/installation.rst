Installation
============

Building from the source::

    git clone https://git.rwth-aachen.de/chem/chemwizard.git
    cd chemwizard/src
    python -m venv env
    source env/bin/activate
    python -m pip install --upgrade pip
    python -m pip install -r requirements.txt
    python -m spacy download en_core_web_trf
    python -m spacy download de_dep_news_trf

Continue
--------

You're all set now! Follow the instructions on :doc:`Using ChemWizard <usage>`.