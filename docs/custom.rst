Custom Crawler
==============
**File:** `custom.py`

This script is for crawling any article you add using the web app, which consists of extracting the article content, analyzing the content, matching entities, creating and inserting entities into the database, using the Helper class.

Independently from the web app, you can also use this script standalone on an article URL this way::

   python custom.py <URL>

.. automodule:: custom
   :members:
   :undoc-members:
   :show-inheritance:
