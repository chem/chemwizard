Atlasdriver
===========
**File:** `atlasdriver.py`

This script provides several methods for accessing the MongoDB and do various actions like getting the updates, inserting the entities and getting the graph.

If you want to test if you've used your connection-string correctly, you can use the following command to check if it raises an ``ConnectionError`` exception::

   python atlasdriver.py

.. automodule:: atlasdriver
   :members:
   :undoc-members:
   :show-inheritance: