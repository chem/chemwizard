Helper
======
**File:** `helper.py`

This script provides several helper functions for crawlers and extraction related operations.

.. automodule:: helper
   :members:
   :undoc-members:
   :show-inheritance:
