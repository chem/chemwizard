CHEMIE TECHNIK Crawler
======================
**File:** `chemietechnik.py`

This script is for crawling the website `CHEMIE TECHNIK <https://www.chemietechnik.de/>`__, which consists of collecting article links, extracting article contents, analyzing contents, matching entities, creating and inserting entities into the database, using the Helper class.

Independently from the web app, you can also use this script standalone to start the crawler process::

   python chemietechnik.py

.. automodule:: chemietechnik
   :members:
   :undoc-members:
   :show-inheritance:
