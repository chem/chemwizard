Welcome to ChemWizard's documentation!
======================================

Check out the source code at our `Gitlab Repository <https://git.rwth-aachen.de/chem/chemwizard>`__.

ChemWizard's documentation is organized into the following sections:

- :ref:`Getting Started<getting-started>`
- :ref:`Code Overview<code-overview>`

.. _getting-started:

Getting Started
===============

- :doc:`Quick Start <quick-start>`
- :doc:`Installation <installation>`
- :doc:`Using ChemWizard <usage>`

.. _code-overview:

Code Overview
=============

- :doc:`Flask Server <wsgi>`
- :doc:`Web App <web>`
- :doc:`MongoDB Atlas Driver <atlasdriver>`
- :doc:`The Helper Instance <helper>`
- :doc:`Patterns for Rule-based matching <patterns>`
- :doc:`CHEManager Crawler <chemanager>`
- :doc:`CHEMIE TECHNIK Crawler <chemietechnik>`
- :doc:`ICIS <icis>`
- :doc:`IHS Markit Crawler <ihsmarkit>`
- :doc:`Custom Crawler <custom>`
- :doc:`Configuring the Config <config>`

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
