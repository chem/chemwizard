Web App
=======

You should check out :doc:`Using ChemWizard <usage>` for getting information about using the web app.

The web app was created with `Bootstrap Studio <https://bootstrapstudio.io/>`__ and uses components from Bootstrap 5.1. If you want to get more information, you can check Bootstrap's documentation `here <https://getbootstrap.com/docs>`__.

The web app alone is actually contained in a single folder ``web``, which you can find in the root directory of the project. The HTML, CSS and JS files inside the ``web`` folder are all minified to reduce the loading time. You can see the structure of the web app in the following::

    web/
    ├── engine.html
    ├── graph.html
    ├── index.html
    ├── stats.html
    └── assets/
        ├── css/
            ├── graph.min.css
            └── styles.min.css
        ├── img/
            ├── building.svg
            ├── droplet-half.svg
            ├── geo-fill.svg
            ├── incognito.svg
            ├── newspaper.svg
            └── person-fill.svg
        └── js/
            ├── engine.min.js
            ├── graph.min.js
            ├── script.min.js
            └── stats.min.js