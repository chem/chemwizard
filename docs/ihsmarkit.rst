IHS Markit Crawler
==================
**File:** `ihsmarkit.py`

This script is for crawling the website `IHS Markit <https://news.ihsmarkit.com/info/press_releases>`__, which consists of collecting article links, extracting article contents, analyzing contents, matching entities, creating and inserting entities into the database, using the Helper class.

Independently from the web app, you can also use this script standalone to start the crawler process::

   python ihsmarkit.py

.. automodule:: ihsmarkit
   :members:
   :undoc-members:
   :show-inheritance:
