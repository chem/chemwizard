Configuring the Config
======================
**File:** `config.json`

This file includes some specifications for the crawlers that affect their working processes. There are 4 different keys, each representing a single website as follows:

* chemanager
* chemietechnik
* icis
* ihsmarkit

In the following we will explain the values of these keys:

page_limit (int)
----------------

This value specifies the number of pages to crawl in the step of collecting article urls. The default is set to 5, so the crawler only goes through the first 5 pages and collect all article urls contained on each page.

article_limit (int)
-------------------

This value specifies the number of articles to be analyzed during the steps of extracting information from articles and analyzing information from articles. The default is set to 5, so the crawler only analyzes the first 5 articles collected.

thread_limit (int)
------------------

**Deprecated**

This value was for specifying the limit of concurrent working threads.

highest_page (int)
------------------

This value specifies the highest paging number of a website. Normally, not all articles are listed in a single page view, instead they use paging to return only some of the results and the user can then request for more content or just go through the paging to access other content that are grouped with an order number. The default is set to 0, so the crawler runs a search on the highest page on the first run.
