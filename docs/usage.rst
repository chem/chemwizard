Using ChemWizard
================

Starting the server
-------------------

After the installation you should still be in the virtual environment. For the first time, you should call the reset script to configure the structure of the database. Use the following commans to reset the database and then start the server::

    python resetDatabase.py
    python -m flask run

Doing so will start the flask server and therefore also the SocketIO server. The program will automatically open the web app in your browser. 

The web app has 3 pages, **Engine**, **Graph** and **Stats**. In the following we will explain what each page is about.

Engine
------

This page is for using the custom crawler on arbitrary articles and also to run the crawler on one of the pre-defined websites. The top card shows the status of each website in the database, which tells us the last date the crawler was run.

In the **Add Article** section you can enter any article link and press the **Add** button to start the crawler. This will open up a log window in the bottom and show some real time output of the crawler while being run. However, you cannot close the log window nor stop the crawler unless the process reaches a halting state.

The **Crawler** section does the same with **Add Article**, but here you have to select a website as an option. This will start the adjusted crawler on the website, which will first collect some articles and then analyze them.

Graph
-----

In this page, you will find a graph visualization of the entire database based on entities. Nodes represent the entities and edges represent a single relationship, where the outgoing edges from articles mean that the article contains another entity. There are 5 different entity types shown in the graph:

* Article
* Organization
* Person
* Chemical
* Location

Double click on an entity to show a card displaying information about the selected entity. 

Stats
-----

This page is solely for displaying some statistics about the database.