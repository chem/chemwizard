Patterns for Rule-based matching
================================
**File:** `patterns.py`

The patterns defined in the file are to be used with the ``Matcher`` class from spaCy. Rule-based matching is method of finding phrases, tokens and matching entities based on pre-defined rules and patterns. Please check the documentation at `Rule-based matching <https://spacy.io/usage/rule-based-matching>`__ before continuing.

You can also check out `this website <https://fginter.github.io/docs/it/pos/all.html>`__ for understanding the POS tags.

There are 12 different entitites defined in the patterns:

- MONEY
- AMOUNT
- ORG
- ORG-LIKE
- LOCATION
- DATE
- YEAR
- PERSON
- CHEMICAL
- CHEMICAL NAME
- PLANT CONSTRUCTION
- PRODUCTION

In the following, we explain each pattern for the entities.

MONEY
-----

This pattern is used to match tokens following these rules:

   Word 1::

      One of these symbols: $, €, £, ¥, ₺

   Word 2::

      A number-like token text

   Word 3 (optional)::

      A SYM token text

   Word 4::

      A NUM or NOUN token text consisting of alphabetic characters
   
   Example::

      $260 million

AMOUNT
------

This pattern is used to match tokens following these rules:

   Word 1::

      A number-like token text

   Word 2::

      A NOUN or PROPN token text

   Word 3 (optional)::

      A SYM token text

   Word 4 (optional)::

      A NOUN or PROPN token text
   
   Example::

      50% share

ORG
---

This pattern is used to match tokens following these rules:

   Word 1 (multiple)::

      - A ORG token text
      - The token text is not a VERB

   Example::
      
      Evonik Industries

ORG-LIKE
--------

This pattern is used to match tokens following these rules:

   Word 1 (optional)::

      - A PROPN token text
      - Uppercase
      - Consists of alphabetic characters
      - The token text is not a GPE, PERSON or an ORG

   Example::

      CEO

LOCATION
--------

This pattern is used to match tokens following these rules:

   Word 1 (multiple)::

      - A GPE token text
      - Titlecase

   Example::

      New Jersey

DATE
----

This pattern is used to match tokens following this regular expression::

   ^\d{1,2}[\.\/]\d{1,2}[\.\/](\d{4}|\d{2})$

Example::

   01.01.1970

YEAR
----

This pattern is used to match tokens following this regular expression::

   ^(19|20)\d{2}$

Example::

   2022

PERSON
------

This pattern is used to match tokens following these rules:

   Word 1 (multiple)::

      - A PERSON token text
      - Titlecase
      - Consists of alphabetic characters

   Example::

      Wolfgang Fahl

CHEMICAL
--------

This pattern is used to match tokens following these rules:

   Word 1::

      - The token text is not an ADP
      - The token text is not a GPE
      - The token text matches the following regular expression:

      ^(\d{0,2}(He|Li|Be|Ne|Na|Mg|Al|Si|Cl|Ar|Ca|Sc|Ti|Cr|Mn|Fe|Co|Ni|Cu|Zn|Ga|Ge|As|Se|Br|Kr|Rb|Sr|Zr|Nb|Mo|Tc|Ru|Rh|Pd|Ag|Cd|In|Sn|Sb|Te|Xe|Cs|Ba|La|Ce|Pr|Nd|Pm|Sm|Eu|Gd|Tb|Dy|Ho|Er|Tm|Yb|Lu|Hf|Ta|Re|Os|Ir|Pt|Au|Hg|Tl|Pb|Bi|Po|At|Rn|Fr|Ra|Ac|Th|Pa|Np|Pu|Am|Cm|Bk|Cf|Es|Fm|Md|No|Lr|Rf|Db|Sg|Bh|Hs|Mt|Ds|Rg|Cn|Nh|Fl|Mc|Lv|Ts|Og|H|B|C|N|O|F|P|S|K|V|Y|I|U|W)\d{0,2}\.?)+$

   Example::

      C2H5OH

CHEMICAL NAME
-------------

This pattern is used to match tokens following these rules:

   Word 1(multiple)::

      - The token text is a NOUN
      - The token text matches the following regular expression:

      (hydrogen|helium|lithium|beryllium|boron|carbon|nitrogen|oxygen|fluorine|neon|sodium|magnesium|aluminium|silicon|phosphorus|sulfur|chlorine|argon|potassium|calcium|scandium|titanium|vanadium|chromium|manganese|iron|cobalt|nickel|copper|zinc|gallium|germanium|arsenic|selenium|bromine|krypton|rubidium|strontium|yttrium|zirconium|niobium|molybdenum|technetium|ruthenium|rhodium|palladium|silver|cadmium|indium|tin|antimony|tellurium|iodine|xenon|caesium|barium|lanthanum|cerium|praseodymium|neodymium|promethium|samarium|europium|gadolinium|terbium|dysprosium|holmium|erbium|thulium|ytterbium|lutetium|hafnium|tantalum|tungsten|rhenium|osmium|iridium|platinum|gold|mercury|thallium|lead|bismuth|polonium|astatine|radon|francium|radium|actinium|thorium|protactinium|uranium|neptunium|plutonium|americium|curium|berkelium|californium|einsteinium|fermium|mendelevium|nobelium|lawrencium|rutherfordium|dubnium|seaborgium|bohrium|hassium|meitnerium|darmstadtium|roentgenium|copernicium|nihonium|flerovium|moscovium|livermorium|tennessine|oganesson)

   Example::
      hydrogen

PLANT CONSTRUCTION
------------------

This pattern is used to match tokens following these rules:

   Word 1 (multiple)::

      - A NOUN token text

   Word 2::

      - The lower token text is 'plant' or 'plants'

   Example::

      hydrogen demonstration plant

PRODUCTION
----------

This pattern is used to match tokens following these rules:

   Word 1::

      - The lower token text is 'production'

   Word 2::

      - The lower token text is 'of'

   Word 3 (multiple)::

      - The token text is ADJ, NOUN or PROPN

   Example::

      production of ethylene oxide