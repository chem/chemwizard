CHEManager Crawler
==================
**File:** `chemanager.py`

This script is for crawling the website `CHEManager <https://www.chemanager-online.com/>`__, which consists of collecting article links, extracting article contents, analyzing contents, matching entities, creating and inserting entities into the database, using the Helper class.

Independently from the web app, you can also use this script standalone to start the crawler process::

   python chemanager.py

.. automodule:: chemanager
   :members:
   :undoc-members:
   :show-inheritance:
