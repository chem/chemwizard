Flask Server
============
**File:** `wsgi.py`

This script serves as a Flask server, which is needed for the communication between the browser and the python script. It uses Socket.IO for sending and receiving messages in real time and do various operations.

Socket.IO is a library that enables low-latency, bidirectional and event-based communication between a client and a server. For the web app, we use the `Socket.IO Javascript Library <https://socket.io/>`__. For the server however, we use the `Flask-SocketIO <https://flask-socketio.readthedocs.io/en/latest/>`__ package for easier compability with the Flask application.

To start the server, you just have to use the following command::

   python -m flask run

This will start the flask server at the address: ``http://127.0.0.1:5000`` This address is hard-coded into the javascript file, which is used by the web app. If you want to expose your ChemWizard server to the public internet, please don't use this flask server as ``Flask’s built-in server is not suitable for production``. Instead, you should use Gunicorn to deploy this app for production. If you have followed the :doc:`Installation <installation>` document, you should have the Gunicorn and other necessary modules already installed on your computer. Simply use the following command to start your Gunicorn web server::

   python -m gunicorn -w 1 -b <local IP address>:<port> wsgi:app

The following methods are called by the server-side event handlers of the SocketIO instance on incoming signals.

.. automodule:: wsgi
   :members:
   :undoc-members:
   :show-inheritance:
