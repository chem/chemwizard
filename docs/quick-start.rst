Quick Start
===========

In this section, you will find all information you need to know to get started using ChemWizard.

Setting up the Database
------------

This project uses MongoDB Atlas service as the database provider. MongoDB Atlas gives a free tier of 512MB space without requiring a credit card. Check out `their website <https://www.mongodb.com/cloud/atlas/register>`__ to register. Select **CLUSTERS > Create a Shared Cluster** to create a cluster. After that, go to **SECURITY > Network Access** on the dashboard. Select **Add IP Address**, click on **ALLOW ACCESS FROM ANYWHERE** and confirm to allow our script to connect to the database. After that create a user if you haven't and give that user database access from the **SECURITY > Database Access** section and give the Built-in Role **Atlas admin** to your user.

On the dashboard again, click on **Connect** and select **Connect your application**. With **Python** as the driver and **3.12 or later** as the version selected, copy the connection string below.

Replace **<password>** with the password for your user. Replace myFirstDatabase with the name of the database that connections will use by default. Ensure any option params are URL encoded.

Now, on your machine use the following command to add this connection string to your environment variables::

    export MONGODB="<replaced-connection-string>"

You can also add this command to the end of your ``.bashrc`` or ``.zshrc`` profile to make it persistent.

Continue
--------

You're almost ready, now follow the instructions on :doc:`Installation <installation>`.