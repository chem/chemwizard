# ChemWizard
 [![pipeline status](https://git.rwth-aachen.de/chem/chemwizard/badges/master/pipeline.svg)](https://git.rwth-aachen.de/chem/chemwizard/-/commits/master)
 
This project contains the final source code for ChemWizard.

---

## Demonstration
![Demo Video](images/demo.m4v)

---

## Documentation
Check out our documentation at [https://chem.pages.rwth-aachen.de/chemwizard/](https://chem.pages.rwth-aachen.de/chemwizard/).

---

## Motivation
The chemical industry aims to assess and reduce environmental impacts of their products. This is a challenging task because the environmental impacts depend various production technologies and local circumstances in the global supply chain. However, currently information about production plants is not available in a structured way. Collecting and structuring the information about chemical production technologies are a necessary prerequisite for a holistic assessment of environmental impacts.

## Task description
In this project, the teams extract information about chemical production plants from news articles. This includes the extractions of chemical compounds, locations of chemical plants, company names, and chemical process technologies. Moreover, the extracted information is linked in a knowledge graph (e.g. link the chemical plants, chemical compounds, and their location in news) and potentially illustrated on a map. The two teams can focus their development on different news webpages and then test their approaches on the remaining news pages.

## News Webpages:
* [IHS Markit](https://news.ihsmarkit.com/)
* [ICIS News](https://www.icis.com/explore/services/market-intelligence/news/)
* [CHEMIE TECHNIK](https://www.chemietechnik.de/)
* [CHEManager](https://www.chemanager-online.com/)

## Useful Links:
* [Knowledge Graphs - Chemical compounds wiki](https://github.com/tholzheim/kgl-exercises/wiki/Chemical-compounds)
* [dbis Knowledge Graphs WS 2021/22 github wiki](https://github.com/tholzheim/kgl-exercises/wiki)
* [PubChem](https://pubchem.ncbi.nlm.nih.gov/)
* [scispacy](https://allenai.github.io/scispacy/)
* [pubchem rdf](https://pubchemdocs.ncbi.nlm.nih.gov/rdf$_4-1)
* [GPT Neo](https://huggingface.co/transformers/model_doc/gpt_neo.html)
* [ChemListem](https://bitbucket.org/rscapplications/chemlistem/src/master/)
* [Apache OpenNLP](http://opennlp.apache.org/)
